import {createSlice} from "@reduxjs/toolkit";
import {Products} from "../../components/DataImport/DataImport";
import CartItem from "../../components/blocks/CartItem/CartItem";


export const cartSlice = createSlice({
    name: 'cart',
    initialState: {
        menu: Products,
        cartItemsArray: [],
        count: 0,
        amount: 0,

    },
    reducers: {
        addToCart: (state, action) => {
            state.menu.forEach(product => {
                if (product.id === action.payload) {
                    state.cartItemsArray.push(product)
                }
            })
        },
        deleteFromCart: (state, action) => {
            if (state.cartItemsArray.length) {
                state.cartItemsArray.splice(action.payload, 1)
            }else {
                state.count = 0;
            }

        },
        getCartItemsCount: (state, action) => {
            if (state.cartItemsArray.length) {
                state.count = state.cartItemsArray.length
            }

        },
        getAmount: (state, action) => {
            if (state.cartItemsArray.length) {
                state.amount = state.cartItemsArray
                    .map(item => item.price)
                    .reduce((previousPrice, currentPrice) => previousPrice + currentPrice)
            } else state.amount = 0
        },
    },
})

export const {
    addToCart,
    deleteFromCart,
    getCartItemsCount,
    getAmount
} = cartSlice.actions

export default cartSlice.reducer