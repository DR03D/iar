import styles from './ProductCardFooterButton.module.css'
import {addToCart, getCartItemsCount, getAmount} from "../../../app/reducers/cart";
import {useDispatch} from "react-redux";

const ProductCardFooterButton = (props) => {
    const dispatch = useDispatch()
    const dispatches = () => {
        dispatch(addToCart(props.id))
        dispatch(getCartItemsCount())
        dispatch(getAmount())
    }
    return (
        <div className={styles['button-wrapper']}>
            <button
                className={styles['button']}
                onClick={() => dispatches()}
            >
                +
            </button>
        </div>

    )
}
export default ProductCardFooterButton