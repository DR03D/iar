import styles from './FormEmailInput.module.css';
const FormEmailInput = (props) => {
  return (
      <div className={styles['input-wrapper']}>
        <input type={'email'} className={styles['email-input']} placeholder={'Email'}/>
      </div>

  )
}
export default FormEmailInput;