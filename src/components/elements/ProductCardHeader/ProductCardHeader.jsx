import styles from './ProductCardHeader.module.css'
const ProductCardHeader = (props) => {
    return (
        <header className={styles['header']}>
            <img src={props.image} alt={props.alt}/>
        </header>
    )
}
export default ProductCardHeader;