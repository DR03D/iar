import styles from './FormPasswordInput.module.css';
const FormPasswordInput = (props) => {
    return (
        <div className={styles['input-wrapper']}>
            <input type={'password'} className={styles['password-input']} placeholder={'Пароль'}/>
        </div>

    )
}
export default FormPasswordInput;