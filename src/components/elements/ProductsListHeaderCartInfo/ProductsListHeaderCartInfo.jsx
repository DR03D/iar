import styles from './ProductsListHeaderCartInfo.module.css'
import { useSelector} from "react-redux";


const ProductsListHeaderCartInfo = (props) => {
    let count = useSelector(state => state.cart.count)
    let amount = useSelector(state => state.cart.amount)

    return (
        <div className={styles['cart-info']}>
            <p>{count} товаров в корзине</p>
            <p>на сумму {amount}₽</p>
        </div>
    )
}
export default ProductsListHeaderCartInfo