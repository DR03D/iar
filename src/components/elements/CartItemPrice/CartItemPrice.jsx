import styles from './CartItemPrice.module.css'
const CartItemPrice = (props) => {
    return (
        <div className={styles['price-wrapper']}>
            <p>{props.price}</p>
        </div>
    )
}
export default CartItemPrice