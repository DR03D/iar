
import styles from './HeaderBackButton.module.css';

const HeaderBackButton = (props) => {
    return (
        <div>
            <button className={styles['back-button']}>{'<'}</button>
        </div>
    )
}
export default HeaderBackButton