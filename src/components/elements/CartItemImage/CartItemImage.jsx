import styles from './CartItemImage.module.css'
const CartItemImage = (props) => {
    return (
        <div className={styles['image-wrapper']}>
            <img src={props.image} alt={props.alt}/>
        </div>
    )
}
export default CartItemImage