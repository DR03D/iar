import styles from "./ProductCardMain.module.css"

const ProductCardMain = (props) => {
  return (
      <div className={styles['body']}>
          <h4>{props.name}</h4>
          <p>{props.desc}</p>
      </div>
  )
}
export default ProductCardMain