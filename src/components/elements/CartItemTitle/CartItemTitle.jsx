import styles from './CartItemTitle.module.css'
const CartItemTitle = (props) => {
    return (
        <div className={styles['title-wrapper']}>
            <h3>{props.name}</h3>
        </div>
    )
}
export default CartItemTitle