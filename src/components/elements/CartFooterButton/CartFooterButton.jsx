import styles from './CartFooterButton.module.css'
const CartFooterButton = (props) => {
  return (
      <div className={styles['button-wrapper']}>
          <button className={styles['button']}>Оформить заказ</button>
      </div>
  )
}
export default CartFooterButton