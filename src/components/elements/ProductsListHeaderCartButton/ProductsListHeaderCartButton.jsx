import cartIcon from './images/Cart.png';
import styles from './ProductsListHeaderCartButton.module.css';
import {Link} from "react-router-dom";

const ProductsListHeaderCartButton = (props) => {
    return (
        <button className={styles['header__cart-button']}>
            <img src={cartIcon} alt={"Cart icon"}/>
        </button>
    )
}
export default ProductsListHeaderCartButton