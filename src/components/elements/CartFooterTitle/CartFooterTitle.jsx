import styles from './CartFooterTitle.module.css'
import {useSelector} from "react-redux";
const CartFooterTitle = (props) => {
    let amount = useSelector(state => state.cart.amount)
    return (
        <div className={styles['title-wrapper']}>
           <h3 className={styles['title']}>Заказ на сумму:</h3>
            <p className={styles['price']}>{amount}₽</p>
        </div>
    )
}
export default CartFooterTitle