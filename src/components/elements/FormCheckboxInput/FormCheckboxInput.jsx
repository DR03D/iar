import styles from './FormCheckboxInput.module.css';

const FormCheckboxInput = (props) => {
    return (
        <label className={styles["container"]}>
            <input type={"checkbox"}/>
            <span className={styles["checkmark"]}></span>
            <p>Я согласен получать обновления на почту</p>
        </label>
)
}
export default FormCheckboxInput
