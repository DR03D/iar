import styles from './CartItemButton.module.css'
import {useDispatch} from "react-redux";
import {getAmount, deleteFromCart} from "../../../app/reducers/cart";
const CartItemButton = (props) => {
    const dispatch = useDispatch();
    const dispatches = () => {
      dispatch(deleteFromCart(props.index));
      dispatch(getAmount())
    }
  return (
      <div className={styles['button-wrapper']}>
          <button
              onClick={()=>dispatches()}
              className={styles['button']}
          >
              x
          </button>
      </div>
  )
}
export default CartItemButton