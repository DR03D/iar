import styles from './ProductsListHeaderTitle.module.css'
import {Link} from "react-router-dom";
import HeaderBackButton from "../HeaderBackButton/HeaderBackButton";

const ProductsListHeaderTitle = (props) => {
    return (
        <div className={styles['header__title']}>
            <div className={styles['back-button-wrapper']}>
                <Link to={'/'}>
                    <HeaderBackButton/>
                </Link>
            </div>
            <div className={styles['title-wrapper']}>
                <h1 >Наша продукция</h1>
            </div>

        </div>

    )
}
export default ProductsListHeaderTitle;