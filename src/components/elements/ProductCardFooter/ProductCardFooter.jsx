import styles from './ProductCardFooter.module.css'

import ProductCardFooterButton from "../ProductCardFooterButton/ProductCardFooterButton";

const ProductCardFooter = (props) => {
  return (
      <footer className={styles['footer']}>
          <div className={styles['footer-info']}>
              <p>{props.price}</p>
              <p>₽</p>
              <p>/</p>
              <p>{props.count}</p>
              <p>гр.</p>
          </div>
          <ProductCardFooterButton id={props.id}/>
      </footer>
  )
}
export default ProductCardFooter