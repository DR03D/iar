import styles from './ProductListHeaderCart.module.css'
import {Link} from "react-router-dom";
import ProductsListHeaderCartButton from "../ProductsListHeaderCartButton/ProductsListHeaderCartButton";
import ProductsListHeaderCartInfo from "../ProductsListHeaderCartInfo/ProductsListHeaderCartInfo";

const ProductListHeaderCart = (props) => {
  return (
      <div className={styles['cart-wrapper']}>
          <ProductsListHeaderCartInfo/>
          <div className={styles['button-wrapper']}>
              <Link to={'/cart'}>
                  <ProductsListHeaderCartButton/>
              </Link>
          </div>
      </div>
  )
}
export default ProductListHeaderCart