import ProductsListHeaderTitle from "../../elements/ProductsListHeaderTitle/ProductsListHeaderTitle";
import styles from './ProductsListHeader.module.css'
import ProductListHeaderCart from "../../elements/ProductListHeaderCart/ProductListHeaderCart";

const ProductsListHeader = (props) => {
    return (
        <div className={styles['header-wrapper']}>
            <header className={styles['header']}>
                <ProductsListHeaderTitle/>
                <ProductListHeaderCart/>
            </header>
        </div>
    )
}
export default ProductsListHeader