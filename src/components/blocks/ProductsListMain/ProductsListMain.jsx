import ProductCard from "../ProductCard/ProductCard";
import styles from './ProductsListMain.module.css'
import {Products} from "../../DataImport/DataImport";

const renderCards = Products.map(product => {
    return (
        <ProductCard
            key={product.id}
            id={product.id}
            name={product.name}
            description={product.description}
            price={product.price}
            count={product.count}
            image={product.image}
            alt={product.alt}/>
    )

})
const ProductsListMain = (props) => {
    return (
        <div className={styles['main']}>
            {renderCards}
        </div>

    )
}
export default ProductsListMain