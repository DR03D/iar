import styles from './CartHeader.module.css'
import HeaderBackButton from "../../elements/HeaderBackButton/HeaderBackButton";
import {Link} from "react-router-dom";

const CartHeader = (props) => {
    return(
        <header className={styles['header']}>
            <div className={styles['button-wrapper']}>
                <Link to={'/products'}>
                    <HeaderBackButton/>
                </Link>
            </div>
           <div className={styles['title-wrapper']}>
               <h1>Корзина с выбранными товарами</h1>
           </div>

        </header>
    )
}
export default CartHeader;