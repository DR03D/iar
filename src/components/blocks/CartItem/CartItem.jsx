import styles from './CartItem.module.css'
import CartItemImage from "../../elements/CartItemImage/CartItemImage";
import CartItemTitle from "../../elements/CartItemTitle/CartItemTitle";
import CartItemPrice from "../../elements/CartItemPrice/CartItemPrice";
import CartItemButton from "../../elements/CartItemButton/CartItemButton";

const CartItem = (props) => {
    return (
        <div className={styles['item-wrapper']}>
            <CartItemImage
                image={props.image}
                alt={props.alt}
            />
            <CartItemTitle
                name={props.name}
            />
            <CartItemPrice
                price={props.price}
            />
            <CartItemButton index={props.index}/>
        </div>
    )
}

export default CartItem