import styles from './ProductCard.module.css'
import ProductCardHeader from "../../elements/ProductCardHeader/ProductCardHeader";
import ProductCardMain from "../../elements/ProductCardMain/ProductCardMain";
import ProductCardFooter from "../../elements/ProductCardFooter/ProductCardFooter";


const ProductCard = (props) => {

    return (
        <div className={styles['card']}>
            <ProductCardHeader id={props.id} image={props.image} alt={props.alt}/>
            <ProductCardMain id={props.id} name={props.name} desc={props.description}/>
            <ProductCardFooter id={props.id} count={props.count} price={props.price}/>
        </div>
    )
}
export default ProductCard