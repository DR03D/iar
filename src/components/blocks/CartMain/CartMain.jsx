import styles from "./CartMain.module.css"
import CartItem from "../CartItem/CartItem";
import {useSelector} from "react-redux";

const CartMain = (props) => {
    const products = useSelector(state => state.cart.cartItemsArray);

    const renderCartItem = products.map((el, index) => {
        return (<CartItem
                index={index}
                key={el.id}
                id={el.id}
                name={el.name}
                description={el.description}
                price={`${el.price}₽`}
                count={el.count}
                image={el.image}
                alt={el.alt}/>)
    })
    return(
        <div className={styles['main-wrapper']}>
            {renderCartItem}

        </div>
    )
}
export default CartMain;