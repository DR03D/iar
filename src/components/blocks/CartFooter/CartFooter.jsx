import CartFooterTitle from "../../elements/CartFooterTitle/CartFooterTitle";
import CartFooterButton from "../../elements/CartFooterButton/CartFooterButton";
import styles from './CartFooter.module.css'
const CartFooter = (props) => {
  return(
      <footer className={styles['footer-wrapper']}>
          <CartFooterTitle/>
          <CartFooterButton/>
      </footer>
  )
}
export default CartFooter;