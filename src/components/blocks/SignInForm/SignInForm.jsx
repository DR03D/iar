import FormTitle from "../../elements/FormTitle/FormTitle";
import FormEmailInput from "../../elements/FormEmailInput/FormEmailInput";
import FormPasswordInput from "../../elements/FormPasswordInput/FormPasswordInput";
import FormCheckboxInput from "../../elements/FormCheckboxInput/FormCheckboxInput";
import FormButton from "../../elements/FormButton/FormButton";
import styles from './SignInForm.module.css';
import {Link} from "react-router-dom";

const SignInForm = (props) => {
    return (
        <form className={styles['login-form']}>
            <FormTitle/>
            <FormEmailInput/>
            <FormPasswordInput/>
            <FormCheckboxInput/>
            <Link to={'/products'}>
                <FormButton/>
            </Link>
        </form>
    )
}
export default SignInForm;