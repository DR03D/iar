import SignInForm from "../../blocks/SignInForm/SignInForm";
import styles from './SignInPage.module.css'

const SignInPage=(props) => {
    return(
        <div className={styles['form-container']}>
            <SignInForm/>
        </div>

    )
}
export default SignInPage