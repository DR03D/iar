import ProductsListHeader from "../../blocks/ProductsListHeader/ProductsListHeader";
import styles from './ProductsListPage.module.css'
import ProductsListMain from "../../blocks/ProductsListMain/ProductsListMain";
const ProductsListPage = (props) => {
    return (
        <div className={styles['products-list-wrapper']}>
            <ProductsListHeader/>
            <ProductsListMain/>

        </div>
    )
}
export default ProductsListPage