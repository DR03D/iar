import CartHeader from "../../blocks/CartHeader/CartHeader";
import CartMain from "../../blocks/CartMain/CartMain";
import CartFooter from "../../blocks/CartFooter/CartFooter";
import styles from './CartPage.module.css'


const CartPage = (props) => {

    return(
        <div className={styles['cart']}>
            <div className={styles['cart-wrapper']}>
                <CartHeader/>
                <CartMain/>
                <CartFooter/>
            </div>
        </div>
    )
}
export default CartPage;