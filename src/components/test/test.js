import {useDispatch, useSelector} from "react-redux";
import {increment, decrement} from "../../app/reducers/counter";

export const Counter = (props) => {
    const count = useSelector((state) => state.counter.value)
    const dispatch = useDispatch()
    return (
        <div>
            <div>
                <button
                    aria-label="Increment value"
                    onClick={() => dispatch(increment())}>+
                </button>
                <div>{count}</div>
                <button
                    aria-label="Decrement value"
                    onClick={() => dispatch(decrement())}>-
                </button>
            </div>
        </div>
    )
}

