import './App.css';
import SignInPage from "./components/pages/SignInPage/SignInPage";
import {Route, Routes} from "react-router-dom";
import ProductsListPage from "./components/pages/ProductsListPage/ProductsListPage";
import CartPage from "./components/pages/CartPage/CartPage";
import {Counter} from "./components/test/test";

function App() {
  return (
    <div className="App">
      <Routes>
          <Route path={'/'} element={<SignInPage/>}/>
          <Route path={'/products'} element={<ProductsListPage/>}/>
          <Route path={'/cart'} element={<CartPage/>}/>
      </Routes>

    </div>
  );
}

export default App;
